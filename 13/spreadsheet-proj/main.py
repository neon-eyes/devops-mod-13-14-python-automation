import openpyxl

inv_file = openpyxl.load_workbook("inventory.xlsx")
product_list = inv_file["Sheet1"]

products_per_supplier = {}
total_value_per_supplier = {}
products_under_10_inv = {}

for prod_row in range(2, product_list.max_row + 1): # iterate through the columns
    supplier_name = product_list.cell(prod_row, 4).value # get value of each cell in column 4 -> D
    inventory = product_list.cell(prod_row, 2).value # get value of each cell in column 2 -> B
    price = product_list.cell(prod_row, 3).value # get value of each cell in column 3 -> C
    product_num = product_list.cell(prod_row, 1).value # get value of each cell in column 1 -> A
    inv_price = product_list.cell(prod_row, 5) # add value in each cell in column 5 -> E

    # calculation for number of products per supplier
    if supplier_name in products_per_supplier:
        products_per_supplier[supplier_name] += 1
    else:
        print(f"adding new supplier: {supplier_name}")
        products_per_supplier[supplier_name] = 1

    # calculation of total value of inventory per supplier
    if supplier_name in total_value_per_supplier:
        current_total_value = total_value_per_supplier.get(supplier_name)
        total_value_per_supplier[supplier_name] = current_total_value = current_total_value + price * inventory
    else:
        total_value_per_supplier[supplier_name] = price * inventory

    # calculate product inventory with less than 10
    if inventory < 10:
        products_under_10_inv[int(product_num)] = int(inventory)

    # add value for total inventory price
    inv_price.value = inventory * price


print(products_per_supplier)
print(total_value_per_supplier)
print(products_under_10_inv)


inv_file.save("inventory_with_total_value.xlsx") # save a copy of the file with changes