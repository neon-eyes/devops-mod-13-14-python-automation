import boto3, schedule
from operator import itemgetter

ec2_client = boto3.client('ec2', region_name="eu-west-3")

snapshots = ec2_client.describe_snapshots(
    OwnerIds=['self']
)['Snapshots']

def delete_volume_snapshots():
    sorted_by_date = sorted(snapshots, key=itemgetter('StartTime'), reverse=True)

    for snap in sorted_by_date[2:]:
        response = ec2_client.delete_snapshot(
            SnapshotId=snap['SnapshotId']
        )
        print(response)

schedule.every(30).seconds.do(delete_volume_snapshots)

while True:
    schedule.run_pending()