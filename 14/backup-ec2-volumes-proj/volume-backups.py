import boto3
import schedule

client = boto3.client('ec2', region_name="eu-west-3")

def create_volume_snapshots():
    volumes = client.describe_volumes(
        Filters=[
            {
                'Name': 'tag:Name',
                'Values': ['prod']
            }
        ]
    )['Volumes']
    for volume in volumes:
        new_snapshot = client.create_snapshot(
            VolumeId=volume['VolumeId']
        )
        print(new_snapshot)


schedule.every(30).seconds.do(create_volume_snapshots)

while True:
    schedule.run_pending()