import requests
import smtplib
import os
import paramiko
from linode_api4 import LinodeClient, Instance
from time import sleep
import schedule


# retrieving email and password for email
EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASS = os.environ.get('EMAIL_PASS')

# linode token
LINODE_TOKEN = os.environ.get('LINODE_TOKEN')

# logic for sending email in case of error
def send_email(msg):
    print('Sending email notification...')
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(EMAIL_ADDRESS, EMAIL_PASS)
        smtp.sendmail(EMAIL_ADDRESS, 'natank98@gmail.com', f'{msg}')


# logic to restart the application - docker container
def restart_container():
    ssh_client = paramiko.SSHClient()
    ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh_client.connect(hostname='139.162.187.222', username='root',key_filename='/home/ethan/.ssh/id_ed25519')
    stdin, stdout, stderr = ssh_client.exec_command('docker start 929e936eac05')
    print(stdout.readlines())
    ssh_client.close()
    print('Application restarted')

# logic to restart server and container
def restart_server_and_container():
    # restart linode server
    print('Rebooting the server...')
    linode_client = LinodeClient(LINODE_TOKEN)
    nginx_server = linode_client.load(Instance, 46329276)
    nginx_server.reboot()
    print('Server rebooted')

    # restart the application
    print('Restarting the application...')
    while True:
        nginx_server = linode_client.load(Instance, 46329276)
        if nginx_server.status == 'running':
            sleep(10)
            restart_container()
            break


# server adderess
url = "http://139-162-187-222.ip.linodeusercontent.com:8080"

def monitor_application():
    try:
        response = requests.get(url=url)
        # if response.status_code == 200:
        if False:
            print("Application is running successfuly!")
        else:
            print(f"Application is DOWN, Fix the problem")
            send_email('Subject: WEBSITE DOWN!\nRestart the server')
            
            # restart the application
            restart_container()
    except Exception as ex:
        print(f'connection error\n{ex}')
        send_email('Subject: WEBSITE DOWN!\nApplication is not accessible at all')
        restart_server_and_container()

# schedule
schedule.every(5).minutes.do(monitor_application)

while True:
    schedule.run_pending()