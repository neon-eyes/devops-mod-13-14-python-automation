module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.13.1"

  cluster_name = "myapp-eks-cluster"
  cluster_version = "1.26"

  subnet_ids = module.myapp-vpc.private_subnets
  vpc_id = module.myapp-vpc.vpc_id

  # worker nodes for dev group (i.e dev environment)  
  eks_managed_node_groups = {
    dev = {
      min_size     = 1
      max_size     = 3
      desired_size = 3

      instance_types = ["t2.micro"]
    }
  }

  tags = {
    environment = "development"
    application = "myapp"
  }
}