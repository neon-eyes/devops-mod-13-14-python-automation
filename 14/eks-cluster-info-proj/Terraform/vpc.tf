provider "aws" {
    region = "eu-central-1"
}

variable "vpc_cidr_block" {}
variable "private_subnet_cidr_block" {}
variable "public_subnet_cidr_block" {}

data "aws_availability_zones" "azs" {
    
}

module "myapp-vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "4.0.1"

  name = "myapp-vpc" # the actual name column of the resource
  cidr = var.vpc_cidr_block
  private_subnets = var.private_subnet_cidr_block # arry of cidr blocks
  public_subnets = var.public_subnet_cidr_block
  azs = data.aws_availability_zones.azs.names

  enable_nat_gateway = true
  single_nat_gateway = true
  enable_dns_hostnames = true

# these tags are for consuption by the k8s cloud controller manager and aws loadbalancer controller
# THESE TAGS ARE REQUIRED !!
  tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
  }
  public_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/elb" = 1 # elb = elastic load balancer
  }
  private_subnet_tags = {
    "kubernetes.io/cluster/myapp-eks-cluster" = "shared"
    "kubernetes.io/role/internal-elb" = 1
  }

}