import boto3

client = boto3.client('eks', region_name="eu-central-1")

clusters = client.list_clusters()['clusters']

for cluster in clusters:
    response = client.describe_cluster(
        name = cluster
    )
    cluster_status = response['cluster']['status']
    cluster_endpoint = response['cluster']['endpoint']
    cluster_version = response['cluster']['version']
    print(f"Cluster {cluster} is {cluster_status}, Endpoint is {cluster_endpoint}, version {cluster_version}")