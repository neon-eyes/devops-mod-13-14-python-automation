import boto3
import schedule

ec2_client = boto3.client('ec2', region_name="eu-west-3")
# ec2_resource = boto3.resource('ec2', region_name="eu-west-3")


def check_instance_status():
    statuses = ec2_client.describe_instance_status()
    for status in statuses['InstanceStatuses']:
        inst_id = status['InstanceId']
        inst_status = status['InstanceStatus']['Status']
        sys_status = status['SystemStatus']['Status']
        state = status['InstanceState']['Name']

        print(f"instance: {inst_id}\ninstance state: {state}\t\tinstance status: {inst_status}\tsystem status: {sys_status}\n")


schedule.every(5).minutes.do(check_instance_status)


while True:
    schedule.run_pending()